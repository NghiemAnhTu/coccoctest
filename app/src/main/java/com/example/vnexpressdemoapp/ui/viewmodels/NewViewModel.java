package com.example.vnexpressdemoapp.ui.viewmodels;

import android.content.Context;
import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.example.vnexpressdemoapp.R;
import com.example.vnexpressdemoapp.ui.entity.NewEntity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class NewViewModel extends BaseViewModel {
    private final static int ITEM_COUNT_LOADING = 10;
    public ArrayList<NewEntity> listNews = new ArrayList<>();
    public MutableLiveData<String> listNewsLoaded = new MutableLiveData<>();
    public String responseNews = "";
    public int indexPage = 1;
    public boolean isEndPage = false;

    public void getListNews(Context context) {
        try {
            if (TextUtils.isEmpty(responseNews)) {

                BufferedReader streamReader = new BufferedReader(
                        new InputStreamReader(context.getResources().openRawResource(R.raw.data), "UTF-8")
                );
                StringBuilder responseStrBuilder = new StringBuilder();
                String inputStr = "";
                inputStr = streamReader.readLine();
                while (inputStr != null) {
                    responseStrBuilder.append(inputStr);
                    inputStr = streamReader.readLine();
                }
                streamReader.close();
                responseNews = responseStrBuilder.toString();
                // load het item -> neu call server thi se chuyen qua load tung phan va them moi vao list trong acticity
                listNews.addAll(new Gson().fromJson(responseStrBuilder.toString(),
                        new TypeToken<ArrayList<NewEntity>>() {
                        }.getType()));

            }

            int endIndex = indexPage * ITEM_COUNT_LOADING;
            if (endIndex > listNews.size()) {
                endIndex = listNews.size();
                isEndPage = true;
            }
            listNewsLoaded.postValue(new Gson().toJson(listNews.subList(0, endIndex)));
            indexPage++;
        } catch (Exception e) {
        }
    }
}
