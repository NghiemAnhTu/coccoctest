package com.example.vnexpressdemoapp.ui.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public abstract class BaseViewModel extends ViewModel {

    private MutableLiveData<String> message = new MutableLiveData<String>();
}
