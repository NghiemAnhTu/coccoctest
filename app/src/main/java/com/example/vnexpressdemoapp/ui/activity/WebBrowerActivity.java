package com.example.vnexpressdemoapp.ui.activity;


import static androidx.webkit.WebSettingsCompat.FORCE_DARK_OFF;
import static androidx.webkit.WebSettingsCompat.FORCE_DARK_ON;

import androidx.appcompat.app.AppCompatActivity;
import androidx.webkit.WebSettingsCompat;
import androidx.webkit.WebViewFeature;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.vnexpressdemoapp.R;
import com.example.vnexpressdemoapp.ui.BaseActivity;

public class WebBrowerActivity extends BaseActivity {

    WebView wvContent;
    public static String ITEM_LINK = "ITEM_LINK";

    public static Intent getIntentWeb(Context context, String link) {
        Intent intent = new Intent(context, WebBrowerActivity.class);
        intent.putExtra(ITEM_LINK, link);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_web_brower;
    }

    @Override
    public void initView() {
        wvContent = findViewById(R.id.wvContent);
        wvContent.getSettings().setJavaScriptEnabled(true);
        wvContent.getSettings().setDomStorageEnabled(true);
        wvContent.getSettings().setAllowFileAccess(true);
        wvContent.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wvContent.getSettings().setLoadsImagesAutomatically(true);
        wvContent.getSettings().setPluginState(WebSettings.PluginState.ON);
        wvContent.getSettings().setUseWideViewPort(true);
        wvContent.getSettings().setUserAgentString("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36");

        String dataLinkHistory = getIntent().getStringExtra(ITEM_LINK);
        if (dataLinkHistory != "") {
            wvContent.loadUrl(dataLinkHistory);
        }

        if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
            switch (getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) {
                case Configuration.UI_MODE_NIGHT_YES:
                    WebSettingsCompat.setForceDark(wvContent.getSettings(), FORCE_DARK_ON);
                    break;
                case Configuration.UI_MODE_NIGHT_NO:
                case Configuration.UI_MODE_NIGHT_UNDEFINED:
                    WebSettingsCompat.setForceDark(wvContent.getSettings(), FORCE_DARK_OFF);
                    break;
            }
        }
    }

    @Override
    public void observer() {
    }

    @Override
    public void initListener() {
    }

    @Override
    public void onBackPressed() {
        if (wvContent.canGoBack()) {
            wvContent.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
