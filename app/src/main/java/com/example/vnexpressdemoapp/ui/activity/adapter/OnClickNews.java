package com.example.vnexpressdemoapp.ui.activity.adapter;

import com.example.vnexpressdemoapp.ui.entity.NewEntity;

public interface OnClickNews {
    void onRead(NewEntity item, int position);
}
