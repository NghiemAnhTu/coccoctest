package com.example.vnexpressdemoapp.ui.activity.adapter;

import android.content.Context;
import android.util.EventLogTags;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.vnexpressdemoapp.R;
import com.example.vnexpressdemoapp.ui.entity.NewEntity;

import java.util.ArrayList;

public class ItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<NewEntity> listItem = new ArrayList();

    private static final int TYPE_DEFAULT = 0;
    private static final int TYPE_NEW = 1;
    private OnClickNews listener;
    private Context context;

    public ItemAdapter(Context context, OnClickNews listener) {
        this.context = context;
        this.listener = listener;
    }

    public void updateList(ArrayList<NewEntity> list) {
        this.listItem = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_NEW:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false));
            default:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).onBind(getItem(position));
        }

    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    private NewEntity getItem(int position) {
        return listItem.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (listItem.get(position) instanceof NewEntity) return TYPE_NEW;
        return TYPE_DEFAULT;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDescription, tvPubDate, tvRead;
        AppCompatImageView image;
        View vLine;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvPubDate = itemView.findViewById(R.id.tvPubDate);
            tvRead = itemView.findViewById(R.id.tvReadMore);
            image = itemView.findViewById(R.id.image);
            vLine = itemView.findViewById(R.id.lineBottom);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onRead((NewEntity) listItem.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }

        public void onBind(Object item) {
            tvTitle.setText(((NewEntity) item).getTitle());
            tvDescription.setText(((NewEntity) item).getDescription());
            tvPubDate.setText(((NewEntity) item).getPubDate());
            Glide.with(context)
                    .load(((NewEntity) item).getImage())
                    .placeholder(R.drawable.ic_image_empty)
                    .error(R.drawable.ic_image_empty)
                    .centerCrop()
                    .into(image);

            if (getAdapterPosition() == listItem.size() - 1) vLine.setVisibility(View.GONE);
            else vLine.setVisibility(View.VISIBLE);


        }
    }
}
