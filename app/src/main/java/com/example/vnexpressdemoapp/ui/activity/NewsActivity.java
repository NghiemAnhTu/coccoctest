package com.example.vnexpressdemoapp.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.CompoundButton;

import com.example.vnexpressdemoapp.R;
import com.example.vnexpressdemoapp.ui.BaseActivity;
import com.example.vnexpressdemoapp.ui.activity.adapter.ItemAdapter;
import com.example.vnexpressdemoapp.ui.activity.adapter.OnClickNews;
import com.example.vnexpressdemoapp.ui.entity.NewEntity;
import com.example.vnexpressdemoapp.ui.viewmodels.NewViewModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class NewsActivity extends BaseActivity implements OnClickNews {

    private ItemAdapter adapter;
    private ArrayList<Object> listItem = new ArrayList<>();
    private ArrayList<NewEntity> listUpdate = new ArrayList<>();
    private RecyclerView listNews;
    LinearLayoutManager layoutManager;
    private SwitchCompat switchUI;

    // so item moi lan loading
    // con 5 item chua hieen thi -> loading theem item vao recyclerview
    private static int ITEM_COUNT_LIMIT = 5;
    public NewViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        model = new ViewModelProviders().of(this).get(NewViewModel.class);
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_news;
    }

    @Override
    public void initView() {
        model.getListNews(this);
        // bind view
        listNews = this.findViewById(R.id.listNews);
        switchUI = findViewById(R.id.switchUI);
        // adapter
        adapter = new ItemAdapter(this, this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        listNews.setAdapter(adapter);
        listNews.setLayoutManager(layoutManager);
    }

    @Override
    public void observer() {
        model.listNewsLoaded.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String newEntities) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (loading.isShowing()) loading.dismiss();
                        listUpdate.clear();
                        listUpdate.addAll(new Gson().fromJson(newEntities, new TypeToken<ArrayList<NewEntity>>() {}.getType()));
                        adapter.updateList(listUpdate);
                    }
                });
            }
        });
    }

    @Override
    public void initListener() {
        switchUI.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                switchEnableDarkUI(isChecked);
            }
        });

        listNews.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // moi lan scroll 10 item -> roi moi load tiep
                // hien thi toi phan tu thu 5 la se lod tiep them tin
                // dy > 0 -> keo xuong duoi
                if (layoutManager.findFirstVisibleItemPosition() >= listUpdate.size() - ITEM_COUNT_LIMIT && dy > 0) {
                    // them 10 phan thu vao list
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!loading.isShowing() && !model.isEndPage) {
                                loading.show();
                                model.getListNews(NewsActivity.this);
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onRead(NewEntity item, int position) {
        // chuyen sang man moi
        startActivity(WebBrowerActivity.getIntentWeb(
                this,
                item.getLink()
        ));
    }
}


