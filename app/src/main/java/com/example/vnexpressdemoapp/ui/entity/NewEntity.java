package com.example.vnexpressdemoapp.ui.entity;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

public class NewEntity {
    @SerializedName("title")
    private String title = "";

    @SerializedName("description")
    private Description description;

    @SerializedName("pubDate")
    private String pubDate = "";

    @SerializedName("link")
    private String link = "";

    @SerializedName("guid")
    private String guid = "";


    @SerializedName("comments")
    private Comment comments;


    public Comment getComments() {
        return comments;
    }

    public void setComments(Comment comments) {
        this.comments = comments;
    }

    public String getDescription() {
        // convert to image, des
        try {
            String result = description.getCdata().substring(description.getCdata().lastIndexOf("</br>"));
            return validateDescription(result);
        } catch (Exception e) {
            // convert loi
        }
        return "";
    }

    public String getImage() {
        int startImage = description.getCdata().lastIndexOf("https");
        int endImage = description.getCdata().lastIndexOf("></a>");
        try {
            String result = description.getCdata().substring(startImage, endImage);
            return validateImage(result);
        } catch (Exception e) {
            // convert loi
        }
        return "";
    }


    public String getTitle() {
        return title;
    }

    public NewEntity setTitle(String title) {
        this.title = title;
        return this;
    }


    public String getPubDate() {
        return pubDate;
    }

    public NewEntity setPubDate(String pubDate) {
        this.pubDate = pubDate;
        return this;
    }

    public String getLink() {
        return link;
    }

    public NewEntity setLink(String link) {
        this.link = link;
        return this;
    }

    public String getGuid() {
        return guid;
    }

    public NewEntity setGuid(String guid) {
        this.guid = guid;
        return this;
    }


    class Comment {
        @SerializedName("__prefix")
        private String prefix = "";

        @SerializedName("__text")
        private String text = "";

    }

    public class Description {
        @SerializedName("__cdata")
        private String cdata = "";

        public String getCdata() {
            return cdata;
        }

        public void setCdata(String cdata) {
            this.cdata = cdata;
        }
    }

    String validateImage(String s) {
        return s.replace(" ", "").replace("\"", "");
    }

    String validateDescription(String s) {
        return s.replaceFirst("</br>", "");
    }
}