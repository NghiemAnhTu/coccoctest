package com.example.vnexpressdemoapp.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.BoringLayout;

import com.example.vnexpressdemoapp.ui.utils.LoadingDialog;
import com.example.vnexpressdemoapp.ui.viewmodels.BaseViewModel;

public abstract class BaseActivity extends AppCompatActivity {
    public static String KEY_DARK_ENABLE = "KEY_DARK_ENABLE";
    public SharedPreferences mSharedPreferences;
    public LoadingDialog loading;
    public Boolean darkEnable = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentLayout());
        loading = new LoadingDialog(this);
        //
        initSharedPreferences();
//        enableDarkUI();
        initView();
        initListener();
        observer();
    }

    public abstract int getContentLayout();

    public abstract void initView();

    public abstract void observer();

    public abstract void initListener();

//    public void enableDarkUI() {
//        // kiem tra dark ui
//        if (mSharedPreferences.getBoolean(KEY_DARK_ENABLE, false)) {
//            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
//        } else {
//            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
//        }
//    }

    public void switchEnableDarkUI(Boolean isEnable) {
        // kiem tra dark ui
        if (isEnable) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        mSharedPreferences.edit().putBoolean(KEY_DARK_ENABLE, isEnable).apply();
    }

    public String getString(String key, String vDefault) {
        if (mSharedPreferences != null) {
            return mSharedPreferences.getString(key, vDefault);
        }
        return "";
    }

    public Boolean getBoolean(String key, Boolean vDefault) {
        if (mSharedPreferences != null) {
            return mSharedPreferences.getBoolean(key, vDefault);
        }
        return false;
    }

    public void initSharedPreferences() {
        if (mSharedPreferences == null)
            this.mSharedPreferences = this.getSharedPreferences("vnexpress", Context.MODE_PRIVATE);
    }
}