# CocCocTest

test junior coccoc

// quyen truy cap internet trong AndroidManifest.xml
  <uses-permission android:name="android.permission.INTERNET"/>
// thêm cac implementation
implementation 'com.github.bumptech.glide:glide:4.11.0'
// gson
implementation 'com.google.code.gson:gson:2.8.5'
implementation "com.squareup.retrofit2:converter-gson:2.5.0"

// web kit
implementation "androidx.webkit:webkit:1.3.0-beta01"

//
  api "androidx.lifecycle:lifecycle-extensions:2.1.0"
  implementation 'androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1'
